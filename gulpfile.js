var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('server', ['sass'], function(){
  browserSync.init({
    server: "./"
  });

  gulp.watch("./scss/*.scss", ['sass']);
  gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('sass', function() {
  return gulp.src("./scss/*.scss")
    .pipe(autoprefixer({
      browsers: ['last 10 versions', 'IE 7']
    }))
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest("./css"))
    .pipe(browserSync.stream());
});

gulp.task('default', ['server']);
